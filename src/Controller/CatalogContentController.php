<?php


namespace Drupal\commerce_product_catalog\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Database\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;


class CatalogContentController extends ControllerBase {

  protected $products_number;

  /**
   * CatalogController constructor.
   */
  public function __construct() {
    $this->products_number = 10;
  }

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->moduleHandler = $container->get('module_handler');
    $instance->pagerManager = $container->get('pager.manager');
    return $instance;
  }


  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function getContent() {

    $tid = \Drupal::routeMatch()->getRawParameter('taxonomy_term');
    $term_products_id = [];
    $cid = 'commerce_product_catalog:' . $tid;

    if ($cache = \Drupal::cache()->get($cid)) {
      $term_products_id = $cache->data;
    }
    else {
      $query = \Drupal::entityQuery('taxonomy_term')
        ->accessCheck(TRUE)
        ->condition('parent', $tid);
      $childs = $query->execute();

      array_unshift($childs, $tid);

      $query = \Drupal::entityQuery('commerce_product')->sort('created', 'DESC')->sort('field_created_date_ubercart', 'DESC');
      $query->condition('field_catalog', $childs, 'IN');
      $term_products_id  = $query->accessCheck()->execute();

      \Drupal::cache()->set($cid, $term_products_id);
    }

    $filter_keys =  \Drupal::request()->query->all();
    unset($filter_keys['page']);

    if (!empty($filter_keys)) {

      $query = \Drupal::entityQuery('commerce_product')->accessCheck();

      // Price filter
      $min_price = !empty($filter_keys['min_price']) ? $filter_keys['min_price'] : NULL;
      $max_price = !empty($filter_keys['max_price']) ? $filter_keys['max_price'] : NULL;
      if (!is_null($min_price) || !is_null($max_price)) {
        $query->condition('variations.entity:commerce_product_variation.price__number', [
          $min_price,
          $max_price
        ], 'BETWEEN');
      }

      // Brand filter
      $brand_tid = !empty($filter_keys['brand']) ? $filter_keys['brand'] : NULL;
      if (!is_null($brand_tid)) {
        $query->condition('field_brend.entity:taxonomy_term.tid', $brand_tid, '=');
      }

      // Criteria filter
      $criteria = !empty($filter_keys['criteria']) ? $filter_keys['criteria'] : '';
      if (!empty($criteria)) {
        $criteria_tids = explode('+', (string) $criteria);
        $query->condition('field_product_criteria.entity:taxonomy_term.tid', $criteria_tids, 'IN');
      }

      /// Sorting field
      $sort_criteria = !empty($filter_keys['sort_field']) ? $filter_keys['sort_field'] : 'date';
      $sort_field = ($sort_criteria== 'price') ? 'variations.entity:commerce_product_variation.price__number' : 'created';

      // Sorting direction
      $sort_direct = !empty($filter_keys['sort_direct']) ? $filter_keys['sort_direct'] : 'ASC';

      $query->sort($sort_field, $sort_direct);
      if ($sort_field == 'date') {
        $query->sort('field_created_date_ubercart', $sort_direct);
      }

      $query_result = $query->execute();
      $term_products_id = array_intersect($query_result, $term_products_id);

    }

    $build['page'] = [
      '#theme' => 'catalog_block',
      '#products'=> $this->buildPager($term_products_id, 10),
      '#cache' => [
        'tags' => ['taxonomy_term_list'],
      ]
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return  $build;
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResultInterface|void
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * Build custom pager.
   */
  private function buildPager($result, $limit = 10) {
    $total = count($result);
    $pager_manager = \Drupal::service('pager.manager');
    $current_page = $pager_manager->createPager($total, $limit)->getCurrentPage();
    $chunks = array_chunk($result, $limit);
    return  \Drupal::service('commerce_product_catalog.get_product_info')->getProductInfo('product_card', $chunks[$current_page]);
  }


}
