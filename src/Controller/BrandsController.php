<?php


namespace Drupal\commerce_product_catalog\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Database;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\BeforeCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\AppendCommand;
use Symfony\Component\HttpFoundation\Request;

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\Core\Url;
use Drupal\Core\Config;
Use Drupal\Core\Config\ConfigFactoryInterface;


class BrandsController extends ControllerBase {

  protected  $products_number;
  protected $config;

  /**
   * CatalogController constructor.
   */
  public function __construct() {
    $this->products_number = 10;
  }

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->moduleHandler = $container->get('module_handler');
    $instance->pagerManager = $container->get('pager.manager');
    return $instance;
  }

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function getContent() {

    $db = Database::getConnection();
    $query = $db->select('taxonomy_term_data', 'ttd');
    $query->condition('ttd.vid', 'brends');
    $query->fields('ttd', ['tid']);
    $tids = $query->execute()->fetchCol();

    $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($tids);
    $a = 1;
    $terms_info = [];
    foreach ($terms as $tid => $term) {
      $media_id_tmp[$tid] = $term->field_brands_image->getValue();
    }
    $a = 1;
    foreach ($terms as $tid => $term) {
      $media_id = $term->field_brands_image->getValue();

      if (!empty($media_id)) {
        $media = Media::load($media_id[0]['target_id']);
        $a = 1;
        $target = (!empty($media) && !empty($media->field_media_image->getValue())) ? $media->field_media_image->getValue() : NULL;

        if (!empty($target)) {
          $image_file = File::load($target[0]['target_id']);
          $image_realpath = \Drupal::service('file_system')
            ->realpath($image_file->getFileUri());
          if (file_exists($image_realpath)) {
            $terms_info[$tid]['image_uri'] = $image_file->getFileUri();
            $terms_info[$tid]['path'] = URL::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $tid])
              ->toString();
          }
        }
      }
    }

    return [
      '#theme' => 'brands_page',
      '#terms'=> $terms_info,
      '#block_title' => 'Лучшие бренды',
    ];

  }


}
