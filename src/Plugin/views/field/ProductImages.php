<?php

/**
 * @file
 * Definition of Drupal\d8views\Plugin\views\field\NodeTypeFlagger
 */

namespace Drupal\commerce_product_catalog\Plugin\views\field;

use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\NodeType;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("commerce_product_catalog_product_images")
 */
class ProductImages extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * @return array|void
   */
  protected function defineOptions() {
    return parent::defineOptions();
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * @param \Drupal\views\ResultRow $values
   *
   * @return \Drupal\Component\Render\MarkupInterface|\Drupal\Core\StringTranslation\TranslatableMarkup|\Drupal\views\Render\ViewsRenderPipelineMarkup|string
   */
  public function render(ResultRow $values) {
    $product_id = $values->product_id;
    $product_info =  \Drupal::service('commerce_product_catalog.get_product_info')-> getProductInfo([$product_id]);
    $a = 1;
    $build = [
       '#theme' => 'product_images',
       '#images' => $product_info[$product_id]['images'],
     ];
    $output = \Drupal::service('renderer')->render($build);
    $a = 1;
    return $output;
  }
}
