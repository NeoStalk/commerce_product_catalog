<?php

namespace Drupal\commerce_product_catalog\Plugin\views\filter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\NumericFilter;
use Drupal\views\ViewExecutable;

/**
 * Filter by start and end date.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("variation_price")
 */
class VariationPriceFilter extends NumericFilter {

  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $a = 1;
    $this->valueTitle = t('Variation price');
#    $this->definition['options callback'] = [$this, 'generateOptions'];
    $this->currentDisplay = $view->current_display;
  }

  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['value'] = [
      'contains' => [
        'min' => ['default' => ''],
        'max' => ['default' => ''],
        'value' => ['default' => ''],
      ],
    ];

    $options['expose']['contains']['placeholder'] = ['default' => ''];
    $options['expose']['contains']['min_placeholder'] = ['default' => ''];
    $options['expose']['contains']['max_placeholder'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultExposeOptions() {
    parent::defaultExposeOptions();

    $this->options['expose']['min_placeholder'] = NULL;
    $this->options['expose']['max_placeholder'] = NULL;
    $this->options['expose']['placeholder'] = NULL;

  }

  /**
   * {@inheritdoc}
   */
  public function buildExposeForm(&$form, FormStateInterface $form_state) {
    parent::buildExposeForm($form, $form_state);
  }

  /**
   * Helper function to generates options.
   *
   * @return array
   *   An array of options.
   */
  public function generateOptions(): array {
/*
    $keys = range(date("Y"), date("Y") - 5);
    $values = range(date("Y"), date("Y") - 5);
    return array_combine($keys, $values);
*/
  }

  /**
   * Helper function to builds the query.
   */
  public function query() {
/*
    if (!empty($this->value)) {
      // Start date like 2021-01-01.
      $start_date_timestamp = DrupalDateTime::createFromArray([
        'year' => $this->value[0],
        'month' => 1,
        'day' => 1,
      ])->getTimestamp();
      // End date like 2021-12-31.
      $start_end_timestamp = DrupalDateTime::createFromArray([
        'year' => $this->value[0],
        'month' => 12,
        'day' => 31,
      ])->getTimestamp();
      $this->query->addWhere('AND', 'node_field_data.created', $this->value, '>=');
      $this->query->addWhere('AND', 'node_field_data.created', $start_end_timestamp, '<=');
    }
*/
  }
}
