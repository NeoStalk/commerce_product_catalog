<?php
namespace Drupal\commerce_product_catalog\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Database;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "hit_sell_block",
 *   admin_label = @Translation("Hit Sell Block"),
 *   category = "Custom"
 * )
 */
class HitSellBlock extends BlockBase {
  public function build() {

    $db = Database::getConnection();
    $query = $db->select('commerce_product__field_category_blocks', 'fcb');
    $query->leftJoin('commerce_product_field_data', 'pfd', 'fcb.entity_id = pfd.product_id');
    $query->orderBy('pfd.created', 'DESC');
    $query->fields('fcb', ['entity_id']);
    $query->condition('fcb.field_category_blocks_target_id', 59);
    $query->range(0,4);

    $products_id = $query->execute()->fetchCol();
    $product_info = \Drupal::service('commerce_product_catalog.get_product_info')->getProductInfo('product_card', $products_id);

    $build = [
      '#theme' => 'catalog_block',
      '#products'=> $product_info,
      '#cache' => [
        'max-age' => -1,
      ]
    ];
    $output = \Drupal::service('renderer')->render($build);
    return [
      '#markup' => $output,
    ];
  }
}
