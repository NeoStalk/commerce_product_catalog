<?php
namespace Drupal\commerce_product_catalog\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Database;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "similar_products_block",
 *   admin_label = @Translation("Similar Productsl Block"),
 *   category = "Custom"
 * )
 */
class SimilarProductsBlock extends BlockBase {
  public function build() {

#    $param = \Drupal::routeMatch()->getParameters();
    $path_param = \Drupal::routeMatch()->getParameter('commerce_product');
    $product_id = $path_param->id();
    $product = \Drupal\commerce_product\Entity\Product::load((int)$path_param->id());
    $product_field_catalog = $product->field_catalog->getValue();
    $catalog_term_tid = reset($product_field_catalog)['target_id'];

    $db = Database::getConnection();
    $query = $db->select('commerce_product__field_catalog', 'cpc');
    $query->leftJoin('commerce_product_field_data', 'pfd', 'cpc.entity_id = pfd.product_id');
    $query->condition('cpc.field_catalog_target_id', $catalog_term_tid);
    $query->condition('cpc.entity_id', $product_id, '<>');
    $query->orderBy('pfd.created', 'DESC');
    $query->range(0,4);
    $query->fields('cpc', ['entity_id']);
    $products_id = $query->execute()->fetchCol();

    $product_info = \Drupal::service('commerce_product_catalog.get_product_info')->getProductInfo('product_card', $products_id);

    return [
      '#theme' => 'catalog_block',
      '#block_title' => 'Похожие товары',
      '#products'=> $product_info,
    ];

  }


  /**
   * @return string[]
   */
  public function  getCacheContexts() {
    return ['url.path'];
  }
}
