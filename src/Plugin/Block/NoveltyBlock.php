<?php
namespace Drupal\commerce_product_catalog\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Database;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "novelty_block",
 *   admin_label = @Translation("Novelty Block"),
 *   category = "Custom"
 * )
 */
class NoveltyBlock extends BlockBase {

  public function build() {

    $db = Database::getConnection();
    $query = $db->select('commerce_product__field_category_blocks', 'fcb');
    $query->leftJoin('commerce_product_field_data', 'pfd', 'fcb.entity_id = pfd.product_id');
    $query->leftJoin('commerce_product__field_created_date_ubercart', 'cdu', 'fcb.entity_id = cdu.entity_id');
    $query->orderBy('cdu.field_created_date_ubercart_value', 'DESC');
    $query->orderBy('pfd.created', 'DESC');
    $query->fields('fcb', ['entity_id']);
    $query->condition('fcb.field_category_blocks_target_id', 278);
    $query->range(0,4);

    $products_id = $query->execute()->fetchCol();
    $a = 1;
    $product_info = \Drupal::service('commerce_product_catalog.get_product_info')->getProductInfo('product_card', $products_id);

    $build = [
      '#theme' => 'catalog_block',
      '#products'=> $product_info,
      '#cache' => [
        'max-age' => -1,
      ]
    ];
/*
    $output = \Drupal::service('renderer')->render($build);
    return [
      '#markup' => $output,
    ];
*/
     return $build;
  }
}
