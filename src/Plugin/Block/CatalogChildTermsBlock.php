<?php
namespace Drupal\commerce_product_catalog\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "catalog_child_terms_block",
 *   admin_label = @Translation("Catalog Child Terms"),
 *   category = "Custom"
 * )
 */
class CatalogChildTermsBlock extends BlockBase {

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function build() {
    $tid = \Drupal::routeMatch()->getRawParameter('taxonomy_term');
    $vid = 'katalog';
    $depth = 1; // 1 to get only immediate children, NULL to load entire tree
    $load_entities = FALSE; // True will return loaded entities rather than ids
    $child_terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid, $tid, $depth, $load_entities);

    $child_terms_links = [];
    foreach ($child_terms as $child_term) {
      $term_name = $child_term->name;
      $term_id = $child_term->tid;
      $url = Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term_id]);
      $child_terms_links[] = Link::fromTextAndUrl($term_name, $url)->toString();
    }

    $build = [
      '#theme' => 'catalog_child_terms',
      '#child_terms_links'=> $child_terms_links,
      '#cache' => [
        'tags' => ['taxonomy_term_list'],
      ]

    ];
    $output = \Drupal::service('renderer')->render($build);

    return [
      '#markup' => $output,
    ];
  }


  /**
   * @return string[]
   */
  public function getCacheContexts() {
    return ['url.path'];
  }

}

