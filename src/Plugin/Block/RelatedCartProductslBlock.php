<?php
namespace Drupal\commerce_product_catalog\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Database;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "related_cart_products_block",
 *   admin_label = @Translation("Related Cart Products"),
 *   category = "Custom"
 * )
 */
class RelatedCartProductslBlock extends BlockBase {

  public function build() {
    \Drupal::service('cache.render')->invalidateAll();
    $build = [];
    $param = \Drupal::routeMatch()->getRouteName();
    $store = \Drupal::service('commerce_store.current_store')->getStore();
    $cart = \Drupal::service('commerce_cart.cart_provider')->getCart('default', $store);
    if (!empty($cart)) {
      $cart_items = $cart->getItems();
      $order_id = $cart->order_id->value;

      if (!empty($cart_items)) {
        $related_products_sku_array = [];
        foreach ($cart_items as $key => $item) {
          $purchased_entity = $item->getPurchasedEntity();
          $product_id = $purchased_entity->getProductId();
          $product = \Drupal\commerce_product\Entity\Product::load($product_id);
          $product_variations = $product->getVariations();
          foreach ($product_variations as $item) {
            $variation_id = $item->id();
            $variation = $variation = \Drupal\commerce_product\Entity\ProductVariation::load($variation_id);
            $variation_sku = $variation->getSku();
            $variation_sku_array = explode('-', (string) $variation_sku ?? '');
            $cart_items_sku[] = $variation_sku_array[0];
          }
          $cart_items_sku = array_unique($cart_items_sku);
          $related_products_sku = $product->field_related_products->getValue();
          $related_products_sku = $related_products_sku[0]['value'];
          $related_products_sku_tmp = explode('+', (string) $related_products_sku ?? '');
          $related_products_sku_array = array_merge($related_products_sku_array, $related_products_sku_tmp);
        }

        $related_products_sku_array = array_unique($related_products_sku_array);
        $related_products_sku_array = array_diff($related_products_sku_array, $cart_items_sku);

        $db = Database::getConnection();
        foreach ($related_products_sku_array as $product_sku) {
          $query = $db->select('commerce_product_variation_field_data', 'vfd');
          $query->condition('vfd.sku', $query->escapeLike($product_sku) . '%', 'LIKE');
          $query->fields('vfd', ['product_id']);
          $query->distinct();
          $product_id_tmp = $query->execute()->fetchCol();
          $products_id[] = reset($product_id_tmp);
        }

        $products = \Drupal::service('commerce_product_catalog.get_product_info')->getProductInfo('table_rows', $products_id);

/*
        if ()
        foreach ($product_info as $key => $item) {
          $product_info[$key]['images'] = !empty($item['images']) ? array_shift($item['images']) : '';
        }
*/
        $a = 1;

        $build = [
          '#theme' => 'related_cart_products',
          '#products' => $products,
        ];
      }
    }
    return $build;
  }

  /**
   * @return int
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
