<?php
namespace Drupal\commerce_product_catalog\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Database;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "related_products_block",
 *   admin_label = @Translation("Related Products"),
 *   category = "Custom"
 * )
 */
class RelatedProductslBlock extends BlockBase {

  public function build() {

#    $param = \Drupal::routeMatch()->getParameters();
    $path_param = \Drupal::routeMatch()->getParameter('commerce_product');
    $product_id = $path_param->id();
    $product = \Drupal\commerce_product\Entity\Product::load((int)$path_param->id());

    $tags = $product->getCacheTags();
    $context = $product->getCacheContexts();
    $this_tags = $this->getCacheTags();
    $this_context = $this->getCacheContexts();
    $tag = 'commerce_product:' . (string) $product_id;
    $a = 1;

    $related_products_sku= $product->field_related_products->getValue();
    $related_products_sku = $related_products_sku[0]['value'];
    $related_products_sku_array = explode('+', (string)$related_products_sku ?? '');

    $db = Database::getConnection();
    foreach ($related_products_sku_array as $product_sku) {
      $query = $db->select('commerce_product_variation_field_data', 'vfd');
      $query->condition('vfd.sku',  $query->escapeLike($product_sku) . '%', 'LIKE');
      $query->fields('vfd', ['product_id']);
      $query->distinct();
      $query_col = $query->execute()->fetchCol();
      $products_id[] = reset($query_col);
    }

    $product_info = \Drupal::service('commerce_product_catalog.get_product_info')->getProductInfo('product_card', $products_id);

    return [
      '#theme' => 'catalog_block',
      '#block_title' => 'Сопутствующие товары',
      '#products'=> $product_info,
    ];

  }

  /**
   * @return string[]
   */
  public function  getCacheContexts() {
    return ['url.path'];
  }

}
