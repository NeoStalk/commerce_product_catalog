<?php
namespace Drupal\commerce_product_catalog\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Database;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "term_description_block",
 *   admin_label = @Translation("Term Description Block"),
 *   category = "Custom"
 * )
 */
class TermDescriptionBlock extends BlockBase {

  /**
   * @return array
   */
  public function build() {

    $tid = \Drupal::routeMatch()->getRawParameter('taxonomy_term');
    $cid = 'term_description_' . $tid;

    if ($cache = \Drupal::cache()->get($cid)) {
      $term_description = $cache->data;
    }
    else {

      $term = \Drupal\taxonomy\Entity\Term::load($tid);
      $term_description = ($term->get('field_description_catalog')) ? $term->get('field_description_catalog')
        ->getValue() : '';
      \Drupal::cache()->set($cid, $term_description);
    }

    return $build = [
      '#theme' => 'term_description_block',
      '#term_description'=> !empty($term_description) ? $term_description[0]['value']   : '',
      '#format' => 'full_html',
      '#cache' => [
        'contexts' => [
          'url'
        ]
      ]
    ];
  }

}
