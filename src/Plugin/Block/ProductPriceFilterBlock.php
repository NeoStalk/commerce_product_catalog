<?php
namespace Drupal\commerce_product_catalog\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "product_price_filter_form",
 *   admin_label = @Translation("Product Price Filter Form"),
 *   category = "Examples"
 * )
 */
class ProductPriceFilterBlock extends BlockBase {
  public function build() {
    return \Drupal::formBuilder()->getForm('Drupal\commerce_product_catalog\Form\ProductPriceFilterForm');
  }
}
