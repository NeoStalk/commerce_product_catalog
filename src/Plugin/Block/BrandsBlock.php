<?php
namespace Drupal\commerce_product_catalog\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "brands_block",
 *   admin_label = @Translation("Brands"),
 *   category = "Custom"
 * )
 */
class BrandsBlock extends BlockBase {

  /**
   * @return array
   */
  public function build() {

    $db = Database::getConnection();
    $query = $db->select('taxonomy_term_data', 'ttd');
    $query->condition('ttd.vid', 'brends');
    $query->range(0,30);
    $query->fields('ttd', ['tid']);
    $tids = $query->execute()->fetchCol();

    $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($tids);

    $terms_info = [];
    foreach ($terms as $tid => $term) {
      $media_id = $term->field_brands_image->getValue();
      $media = Media::load($media_id[0]['target_id']);
      $target = !empty($media->field_media_image->getValue()) ? $media->field_media_image->getValue() : NULL;

      if (!empty($target)) {
        $image_file = File::load($target[0]['target_id']);
        $terms_info[$tid]['image_uri'] = $image_file->getFileUri();
        $terms_info[$tid]['path'] = URL::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $tid])->toString();
      }
    }

    return [
      '#theme' => 'brands_block',
      '#terms'=> $terms_info,
      '#block_title' => 'Популярные бренды'
    ];
  }


  /**
   * @return string[]
   */
/*
  public function getCacheContexts() {
    return ['url.path'];
  }
*/
}

