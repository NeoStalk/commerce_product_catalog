<?php

namespace Drupal\commerce_product_catalog;


use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\commerce_product\Entity\Product;

/**
 * Class GetProductImages
 *
 * @package Drupal\commerce_product_catalog
 */
class GetProductImages {

  public function getProductImages($product_id) {
    $product = Product::load($product_id);
    $medias_id = [];
    //Собираем Media по полю в товаре
    $product_images = $product->field_product_images->getValue();

    if (!empty($product_images)) {
      foreach ($product_images as $product_image_item) {
        $medias_id[] = $product_image_item;
      }
    }

    //Собираем Media по вариациям товара
    $variations = $product->getVariations();
    if (!empty($variations)) {
      foreach ($variations as $key => $variation_item) {
        $product_price[] = (int) $variation_item->getPrice()->getNumber() + 0;
        $variation_id = $variation_item->get('variation_id')->getValue()[0]['value'];
        $variation = \Drupal\commerce_product\Entity\ProductVariation::load($variation_id);
        $media_id_item = $variation->field_product_images->getValue();
        $medias_id[] = array_shift($media_id_item);
      }
    }

    //Получаем массив картинок из всех Media
    $image_files = [];
    foreach ($medias_id as $media_item_id) {
      if (!is_null($media_item_id)) {
        $media = Media::load($media_item_id['target_id']);
        $file_fid = $media->field_media_image->getValue();
        $file = File::load($file_fid[0]['target_id']);
        $image_files[] = $file->getFileUri();
      }
    }

    return $image_files;
  }

}
