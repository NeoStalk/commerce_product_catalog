<?php

namespace Drupal\commerce_product_catalog\Form;

use Drupal\ajax_command_page_reload\Ajax\PageReloadCommand;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class ProductPriceFilterForm extends FormBase {

  /**
   * @return string
   */
  public function getFormId() {
    return 'commerce_product_catalog_product_price_filter_form';
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $current_min_price = !empty($_GET['min_price']) ? $_GET['min_price'] : NULL;
    $current_max_price = !empty($_GET['max_price']) ? $_GET['max_price'] : NULL;
    $current_criteria = !empty($_GET['criteria']) ? $_GET['criteria'] : NULL;
    $current_sort_direction = !empty($_GET['sort_direct']) ? $_GET['sort_direct'] : NULL;
    $current_brand_term = !empty($_GET['brand']) ? $_GET['brand'] : NULL;

    $tid = \Drupal::routeMatch()->getRawParameter('taxonomy_term');

    $db = Database::getConnection();
    $query = \Drupal::entityQuery('taxonomy_term')
      ->accessCheck(TRUE)
      ->condition('parent', $tid);
    $result_query = $query->execute();

    $childs = [];
    $childs[] = $tid;
    if (!empty($result_query)) {
      foreach ($result_query as $child_item) {
        $childs[] = $child_item;
      }
    }

    // Price field
    $query = $db->select('commerce_product_variation_field_data', 'vfd');
    $query->leftJoin('commerce_product__field_catalog', 'fc', 'fc.entity_id = vfd.product_id');
    $query->condition('fc.field_catalog_target_id', $childs, 'IN');
    $query->addExpression('MIN(vfd.price__number)', 'min_price');
    $query->addExpression('MAX(vfd.price__number)', 'max_price');
    $query_range = $query->execute()->fetchAll();
    $query_range = array_shift($query_range);
    $min_price = $query_range->min_price + 0;
    $max_price = $query_range->max_price + 0;

    $form['price_range'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Цена'),
    ];

    $form['price_range']['price_slider'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'id' => 'price-slider'
      ],
    ];

    $form['price_range']['price_slider_result'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'id' => 'price-result',
      ],
    ];


    if (is_null($current_min_price)) {
      $current_min_price = $min_price;
    };

    $form['price_range']['min_price'] =[
      '#type' => 'textfield',
      '#title' => $this->t('min'),
      '#default_value' => $current_min_price,

      '#ajax' => [
        'callback' => '::ajaxSubmitForm',
        'event' => 'submit',
        'speed' => 'fast'
      ]
    ];

    if (is_null($current_max_price)) {
      $current_max_price = $max_price;
    };

    $form['price_range']['max_price'] =[
      '#type' => 'textfield',
      '#title' => $this->t('max'),
      '#default_value' => $current_max_price,
       '#ajax' => [
        'callback' => '::ajaxSubmitForm',
        'event' => 'submit',
        'speed' => 'fast'
      ]

    ];

    // Brand field
    $query = $db->select('commerce_product__field_brend', 'fb');
    $query->leftJoin('commerce_product__field_catalog', 'fc', 'fc.entity_id = fb.entity_id');
    $query->condition('fc.field_catalog_target_id', $childs, 'IN');
    $query->fields('fb', ['field_brend_target_id']);
    $query_result = $query->distinct()->execute()->fetchCol();

    $options = [];
    if (!empty($query_result)) {
      foreach ($query_result as $brand_term_id) {
        $brand_term = \Drupal\taxonomy\Entity\Term::load($brand_term_id);
        if (!empty($brand_term)) {
          $options[$brand_term_id] = $brand_term->getName();
        }
      }
    }


    asort($options);
    $options =  array(0 => '- ЛЮБОЙ -') + $options;

    $form['brand'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Бренд'),
    ];

    if (is_null($current_brand_term)) {
      $current_brand_term = 0;
    }
    $form['brand']['brand_term'] = [
      '#type'=> 'select',
      '#options' => $options,
      '#default_value' => $current_brand_term,

      '#ajax' => [
        'callback' => '::ajaxSubmitForm',
        'event' => 'change',
        'speed' => 'fast'
      ]

    ];

    // Sorting field
    $options = [
      'date' => 'По актуальности',
      'price' => 'По цене'
    ];

    $form['sorting_field'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Сортировать'),
    ];

    $current_sort_field = !empty($_GET['sort_field']) ? $_GET['sort_field'] : 'date';

    $form['sorting_field']['sort_field'] = [
      '#type'=> 'select',
      '#options' => $options,
      '#default_value' => $current_sort_field,
      '#ajax' => [
        'callback' => '::ajaxSubmitForm',
        'event' => 'change',
        'speed' => 'fast'
      ]
    ];

    // Sorting direction
    $options = [
      'DESC' => 'По убыванию',
      'ASC' => 'По возрастанию'
    ];

    $form['sorting_direction'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Сортировать'),
    ];

    $current_sort_direction = !empty($_GET['sort_direct']) ? $_GET['sort_direct'] : 'ASC';

    $form['sorting_direction']['sort_direct'] = [
      '#type'=> 'select',
      '#options' => $options,
      '#default_value' => $current_sort_direction,
      '#ajax' => [
        'callback' => '::ajaxSubmitForm',
        'event' => 'change',
        'speed' => 'fast'
      ]
    ];


    // Criteria field
    $query = $db->select('taxonomy_term__parent', 'ttp');
    $query->leftJoin('commerce_product__field_product_criteria', 'fpc', 'fpc.field_product_criteria_target_id = ttp.entity_id');
    $query->leftJoin('commerce_product__field_catalog', 'fc', 'fc.entity_id = fpc.entity_id');
    $query->condition('fc.field_catalog_target_id', $childs, 'IN');
    $query->condition('ttp.bundle', 'product_criteria');
    $query->fields('ttp', ['entity_id','parent_target_id']);
    $query_result = $query->execute()->fetchAll();
    $query_result_1 = $query->execute()->fetchCol(1);
    $query_result_unique = array_unique($query_result_1);

    $parent_children = [];
    if (!empty($query_result_unique)) {
      foreach ($query_result_unique as $parent_tid) {
        $parent_children[$parent_tid] = [];
        foreach ($query_result as $item) {
          if ($item->parent_target_id == $parent_tid && !in_array($item->entity_id, $parent_children[$parent_tid])) {
            $parent_children[$parent_tid][] = $item->entity_id;
          }
        }
      }
    }

    $form['product_criteria'] = [
      '#title' => t('Дополнительные параметры'),
      '#type' => 'details',
      '#tree' => TRUE,
      '#open' => FALSE
    ];

    $current_criteria_tids =[];
    if (!is_null($current_criteria)) {
      $current_criteria_tids = explode('+', (string)$current_criteria ?? '');
    }
    if (!empty($parent_children)) {
      foreach ($parent_children as $parent_tid => $children_tids) {
        if ($parent_tid != 0) {
          $parent_term = \Drupal\taxonomy\Entity\Term::load($parent_tid);
          $parent_label = $parent_term->getName();
        }
        else {
          $parent_label = (string) '';
        }
        $options = [];
        $current_tid = [];
        foreach ($children_tids as $child_tid) {
          $child_term = \Drupal\taxonomy\Entity\Term::load($child_tid);
          $options[$child_tid] = $child_term->getName();
          if (in_array($child_tid, $current_criteria_tids)) {
            $current_tid[] = $child_tid;
          }
        }

        $form['product_criteria']['term_' . $parent_tid] = [
          '#type' => 'checkboxes',
          '#title' => t($parent_label),
          '#options' => $options,
          '#default_value' => $current_tid,
          '#ajax' => [
            'callback' => '::ajaxSubmitForm',
            'speed' => 'fast'
          ]
        ];
      }
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Сбросить'),
      '#attributes' => [
        'class' => ['reset-form']
      ],
      '#prefix' => "<div class='d-md-flex justify-content-center'>",
      '#suffix' => "</div>",
      '#ajax' => [
        'callback' => [$this,  'resetForm'],
        'event' => 'click',
        'speed' => 'fast'
      ]

    ];

    $form['#attached']['library'][] = 'commerce_product_catalog/custom';
    $form['#attached']['drupalSettings']['min_price'] = $min_price;
    $form['#attached']['drupalSettings']['max_price'] = $max_price;
    $form['#attached']['drupalSettings']['current_min_price'] = $current_min_price;
    $form['#attached']['drupalSettings']['current_max_price'] = $current_max_price;

    $form['#cache']['tags'] = ['taxonomy_term_list'];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Nothing.

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {


  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function ajaxSubmitForm(array &$form, FormStateInterface $form_state) {

    $route_name = \Drupal::routeMatch()->getRouteName();
    $current_path = \Drupal::service('path.current')->getPath();
    $values = $form_state->getValues();
    $criteria = [];
    foreach ($values['product_criteria'] as $parent_item ) {
      foreach ($parent_item as $child_item) {
        if ($child_item !=0) {
          $criteria[] = $child_item;
        }
      }
    }

    $path_params = [];
    $path_params['min_price'] = $values['min_price'];
    $path_params['max_price'] = $values['max_price'];

    if (count($criteria) > 0) {
      $path_params['criteria'] = implode('+', $criteria);;
    }

    if ($values['brand_term'] != 0) {
      $path_params['brand'] = $values['brand_term'];
    }

    $path_params['sort_field'] = $values['sort_field'] ;
    $path_params['sort_direct'] = $values['sort_direct'] ;

    $url = Url::fromUserInput($current_path, ['query' => $path_params])->toString();
    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand($url));

    return $response;
  }

  public function resetForm(array &$form, FormStateInterface $form_state) {
    $current_path = \Drupal::service('path.current')->getPath();
    $page = $_GET['page'];
    $path_params = [];
    if (!is_null($page)) {
      $path_params['page'] = $page;
    }
    $url = Url::fromUserInput($current_path, ['query' => $path_params])->toString();
    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand($url));

    return $response;
  }
}
