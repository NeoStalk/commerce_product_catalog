<?php

namespace Drupal\commerce_product_catalog;

use Drupal\Core\Database\Database;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\commerce_product\Entity\Product;

/**
 * Class GetProductInfo
 *
 * @package Drupal\commerce_product_catalog
 */
class GetProductInfo {

  /**
   * @param $theme
   * @param $product_ids
   *
   * @return array|null
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getProductInfo($theme, $product_ids) {
    if (!empty($product_ids)) {
      $products_info = [];
      $products = Product::loadMultiple($product_ids);

      foreach ($products as $product) {

        $product_id = $product->id();
        $product_link = $product->toLink();
        $product_variations = $product->getVariations();
        $product_price = (int) $product_variations[0]->getPrice()->getNumber() + 0;
        $product_rating = $product->field_product_rating->view();

        $cart_button = [
          '#lazy_builder' => [
            'commerce_product.lazy_builders:addToCartForm',
            [
              $product_id,
              'tmp',
              TRUE,
              'ru',
            ],
          ],
          '#create_placeholder' => TRUE,
        ];

        $images = [];

        if ($theme === 'product_card') {
          $a = 1;
          $images = [
            '#lazy_builder' => [
              'commerce_product_catalog.lazy_builders:OwlCarouselRender',
              [
                $product_id,
              ],
            ],
            '#create_placeholder' => TRUE,
          ];
        }

        if ($theme === 'table_rows') {
          $product_images = \Drupal::service('commerce_product_catalog.get_product_images')
            ->getProductImages($product_id);
          $images = !empty($product_images) ? array_shift($product_images) : '';
        }

        $products_info[$product_id] = [
          '#theme' => $theme,
          '#images' => $images,
          '#product_id' => $product_id,
          '#product_link' => $product_link,
          '#product_price' => $product_price . ' руб.',
          '#cart_button' => $cart_button,
          '#product_rating' => $product_rating,

        ];
      }

      return $products_info;
    }
    else {
      return NULL;
    }
  }

}
