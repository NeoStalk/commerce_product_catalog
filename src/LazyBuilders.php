<?php

namespace Drupal\commerce_product_catalog;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Class LazyBuilders
 *
 * @package Drupal\commerce_product_catalog
 */
class LazyBuilders implements TrustedCallbackInterface {

  /**
   * @param $product_id
   *
   * @return array
   */
  public function OwlCarouselRender($product_id) {

    $images = \Drupal::service('commerce_product_catalog.get_product_images')->getProductImages($product_id);
    return [
      '#theme' => 'owl_image_carousel',
      '#images'=> $images,
      '#attached' => [
        'library' => [
          'commerce_product_catalog/custom',
        ],
      ]
    ];

  }

  /**
   * @return string[]
   */
  public static function trustedCallbacks() {
    return ['OwlCarouselRender'];
  }
}
