/**
 * @file
 * bef_sliders.js
 *
 * Adds jQuery UI Slider functionality.
 */

(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.commerce_product_catalog = {
    attach: function (context, settings) {
      // Refresh cart block
      $('.cart--cart-block').on('click', function () {
          $('.view-commerce-cart-block').trigger('RefreshView');
/*          console.log('!!!!!!!!!!!!!!!!!!!!!!!!1');*/
      });

      // Price slider
      $("#edit-min-price").attr('value', drupalSettings['current_min_price']);
      $("#edit-max-price").attr('value', drupalSettings['current_max_price']);

      $("#price-slider").slider({
        range: true,
        min: drupalSettings['min_price'],
        max: drupalSettings['max_price'],
        values: [drupalSettings['current_min_price'], drupalSettings['current_max_price']],
        slide : function(event, ui) {
          $("#price-result").text( "от " + ui.values[0] + " до " + ui.values[1] );
        },

        stop : function(event, ui) {
          console.log('cur_-min_price = ' + parseInt(drupalSettings['current_min_price']));
          console.log('ui.values_0 = ' + parseInt(ui.values[0]));
          console.log('cur_max_price = ' + parseInt(drupalSettings['current_max_price']));
          console.log('ui.values_1 = ' + parseInt(ui.values[1]));


          if (parseInt(drupalSettings['current_min_price']) != parseInt(ui.values[0])) {
            $("#edit-min-price").attr('value', ui.values[0]).submit();
          }
          if (parseInt(drupalSettings['current_max_price']) != parseInt(ui.values[1])) {
            $("#edit-max-price").attr('value', ui.values[1]).submit();
          }
          console.log('MIN = ' + $("#edit-min-price").val());
          console.log('MAX = ' + $("#edit-max-price").val());
        }
      });

      $( "#price-result" ).text("от " + $("#price-slider").slider("values", 0) + " до " + $("#price-slider").slider("values", 1));
    }
  }
})(jQuery, Drupal, drupalSettings, once);
