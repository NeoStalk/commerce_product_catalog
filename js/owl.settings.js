  jQuery(document).ready(function ($) {
    jQuery(".owl-image-slider").each(function () {
      jQuery(this).owlCarousel({
        items: 1,
        loop: true,
        nav: false,
        autoplay: false,
        responsive: false,
      });
    });

    let owl_child_terms = $('.owl-child-terms');
    owl_child_terms.owlCarousel({
      loop:true,
      margin:10,
      nav:false,
      responsiveClass:true,
      autoplay:false,
      items:2,
    });
  });


